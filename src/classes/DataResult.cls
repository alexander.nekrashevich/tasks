public with sharing class DataResult {
    public List<PrivateContact> changedPrivateContacts = new List<PrivateContact>();
    public List<PrivateContact> nonChangedPrivateContacts = new List<PrivateContact>();
    public List<PublicContact> changedPublicContacts = new List<PublicContact>();
}