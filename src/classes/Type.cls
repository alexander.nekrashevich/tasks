public with sharing abstract class Type {
    protected String contactType;
    protected Double amountOfMoney;

    public String getContactType() {
        return contactType;
    }
}