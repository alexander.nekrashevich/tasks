public class Example5 {
    public static void deleteContactsWithUniqueName() {
        Map<String, Contact> uniqueName_Contact = new Map<String, Contact>();

        for (Contact c: [SELECT Id, Name, Phone, Email FROM Contact LIMIT 50000]) {
            if (uniqueName_Contact.get(c.Name) == null) {
                uniqueName_Contact.put(c.Name, c);
            }
        }

        delete uniqueName_Contact.values();
    }

    public static void deleteLeadsWithUniqueName() {
        Map<String, Lead> uniqueName_Lead = new Map<String, Lead>();

        for (Lead lead: [SELECT Id, Name, Phone, Email, Title FROM Lead]) {
            if (uniqueName_Lead.get(lead.Name) == null) {
                uniqueName_Lead.put(lead.Name, lead);
            }
        }

        delete uniqueName_Lead.values();
    }

    public static Boolean deleteSObjectsWithUniqueName(String sObjectName) {
        Map<String, Schema.SObjectType> objects = Schema.getGlobalDescribe();
        Map<String, SObject> uniqueName_SObject = new Map<String, SObject>();

        if (objects.get(sObjectName) == null) {
            return false;
        }

        for (SObject sobj: Database.query('SELECT Id, Name FROM ' + sObjectName + ' LIMIT 50000')) {
            String name = String.valueOf(sobj.get('Name'));

            if (uniqueName_SObject.get(name) == null) {
                uniqueName_SObject.put(name, sobj);
            }
        }

        delete uniqueName_SObject.values();
        return true;
    }
}