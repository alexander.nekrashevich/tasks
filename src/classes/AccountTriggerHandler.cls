public with sharing class AccountTriggerHandler {
    public static void afterUpdateHandler(Map<Id, Account> newAccountsMap, Map<Id, Account> oldAccountsMap) {
        Map<Id, Contact> contactsByIds = new Map<Id, Contact>([SELECT Id, AccountId, Account_LastName__c FROM Contact WHERE AccountId IN :newAccountsMap.keySet()]);
        List<Contact> contactsToUpdate = new List<Contact>();

        for (Account newAcc: newAccountsMap.values()) {
            Account oldAcc = oldAccountsMap.get(newAcc.Id);

            if (newAcc.Name != oldAcc.Name) {
                List<Contact> relatedContacts = extractRelatedContactsByAccountId(newAcc.Id, contactsByIds.values());
                for (Contact cont: relatedContacts) {
                    cont.Account_LastName__c = newAcc.Name + ' ' + newAcc.Id;
                }
                contactsToUpdate.addAll(relatedContacts);
            }
        }

        update contactsToUpdate;
    }

    private static List<Contact> extractRelatedContactsByAccountId(Id accId, List<Contact> contacts) {
        List<Contact> relatedContacts = new List<Contact>();

        for (Contact cont: contacts) {
            if (cont.AccountId == accId) {
                relatedContacts.add(cont);
            }
        }

        return relatedContacts;
    }
}