public with sharing class LeadTriggerHandler {
    public static void createSEOAndCTOforConvertedAccount(List<Lead> leads) {
        List<Contact> contacts = new List<Contact>();
        List<Id> accountIds = new List<Id>();

        for (Lead lead: leads) {
            if(lead.IsConverted) {
                accountIds.add(lead.ConvertedAccountId);
            }
        }

        for (Account acc: [SELECT Id, Name FROM Account WHERE Id IN :accountIds LIMIT 50000]) {
            Contact seo = new Contact(LastName = 'SEO ' + acc.Name);
            seo.AccountId = acc.Id;
            seo.isSEO__c = true;

            Contact cto = new Contact(LastName = 'CTO ' + acc.Name);
            cto.AccountId = acc.Id;
            cto.isCTO__c = true;

            contacts.add(seo);
            contacts.add(cto);
        }

        insert contacts;
    }
}