public with sharing class ContactImpl implements ProfitAssignee {

    private String name;
    private Decimal profit;

    public ContactImpl(String name, Decimal profit) {
        this.name = name;
        this.profit = profit;
    }

    public Decimal calculateProfit() {
        return profit == null ? 0 : profit;
    }
}