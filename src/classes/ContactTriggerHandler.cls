public with sharing class ContactTriggerHandler {

    public static Boolean isRickContactsInserted = false;

    public static void handleTrigger(
            List<Contact> newContacts,
            List<Contact> oldContacts,
            Map<Id, Contact> newContactsMap,
            Map<Id, Contact> oldContactsMap,
            System.TriggerOperation triggerEvent
    ) {

        switch on triggerEvent {
            when AFTER_INSERT {
                connectRickAndMorty(newContacts);
            }
            when BEFORE_INSERT, BEFORE_UPDATE {
                seoAndCtoHandler(newContacts);
                fillAccountLastNameField(newContacts);
                sadMortyHandler(newContacts);
            }
            when BEFORE_DELETE {
                beforeDeleteHandler(oldContacts);
            }
        }
    }

    public static void beforeDeleteHandler(List<Contact> contacts) {
        Set<Id> accIds = new Set<Id>();

        for (Contact cont: contacts) {
            if (cont.isCTO__c || cont.isSEO__c) {
                accIds.add(cont.AccountId);
            }
        }

        Map<Id, Account> accountsByIds = new Map<Id, Account>([SELECT Id, isExistSEO__c, isExistCTO__c FROM Account WHERE Id IN :accIds LIMIT 50000]);
        for (Contact cont: contacts) {
            Id accId = cont.AccountId;

            if (cont.isSEO__c) {
                accountsByIds.get(accId).isExistSEO__c = false;
            }

            if (cont.isCTO__c) {
                accountsByIds.get(accId).isExistCTO__c = false;
            }
        }

        update accountsByIds.values();
    }

    public static void seoAndCtoHandler(List<Contact> contacts) {
        Set<Id> accIds = new Set<Id>();

        for (Contact cont: contacts) {
            accIds.add(cont.AccountId);
        }

        Map<Id, Account> accountsByIds = new Map<Id, Account>([SELECT Id, isExistSEO__c, isExistCTO__c FROM Account WHERE Id IN :accIds LIMIT 50000]);

        for (Contact cont: contacts) {
            if (cont.isSEO__c) {
                accountsByIds.get(cont.AccountId).isExistSEO__c = true;
            }

            if (cont.isCTO__c) {
                accountsByIds.get(cont.AccountId).isExistCTO__c = true;
            }
        }

        update accountsByIds.values();
    }

    public static void fillAccountLastNameField(List<Contact> contacts) {
        List<Id> contactIds = getContactIdsFromList(contacts);
        Map<Id, Account> accountsByIds = new Map<Id, Account>([SELECT Id, Name FROM Account WHERE Id IN :contactIds LIMIT 50000]);

        for (Contact cont: contacts) {
            if (cont.AccountId != null) {
                Account acc = accountsByIds.get(cont.AccountId);
                cont.Account_LastName__c = acc.Name + ' ' + acc.Id;
            } else {
                cont.Account_LastName__c = '';
            }
        }
    }

    private static List<Id> getContactIdsFromList(List<Contact> contacts) {
        List<Id> ids = new List<Id>();

        for (Contact contact: contacts) {
            if (contact.AccountId != null) {
                ids.add(contact.AccountId);
            }
        }

        return ids;
    }

    public static void connectRickAndMorty(List<Contact> contacts) {
        List<Id> mortyIds = new List<Id>();
        List<Id> rickIdsWithRelatedMorty = getRickIdsWithRelatedMorty();
        List<Contact> rickContacts = [SELECT Id FROM Contact WHERE FirstName = 'Rick' AND LastName = 'Sanchez' AND Id NOT IN :rickIdsWithRelatedMorty];

        for (Contact contact: contacts) {
            if (isMorty(contact)) {
                mortyIds.add(contact.Id);
            }
        }

        List<Contact> mortyContacts = [SELECT Id FROM Contact WHERE Id IN :mortyIds LIMIT 50000];

        List<Contact> toUpdate = new List<Contact>();
        while (rickContacts.size() != 0 && mortyContacts.size() != 0) {
            Contact rick = rickContacts.remove(0);
            Contact morty = mortyContacts.remove(0);

            morty.MyRick__c = rick.Id;
            toUpdate.add(morty);
        }

        update toUpdate;
    }

    public static void sadMortyHandler(List<Contact> contacts) {
        for (Contact cont: contacts) {
            if (isMorty(cont) && cont.MyRick__c == null) {
                cont.SadMorty__c = true;
            } else {
                cont.SadMorty__c = false;
            }
        }
    }

    private static Boolean isRick(Contact cont) {
        if (cont.FirstName == 'Rick' && cont.LastName == 'Sanchez') {
            return true;
        } else {
            return false;
        }
    }

    private static Boolean isMorty(Contact cont) {
        if (cont.FirstName == 'Morty' && cont.LastName == 'Smith') {
            return true;
        } else {
            return false;
        }
    }

    private static List<Id> getRickIdsWithRelatedMorty() {
        List<Id> ids = new List<Id>();

        for (Contact cont: [SELECT Id FROM Contact WHERE MyRick__c != null]) {
            ids.add(cont.id);
        }

        return ids;
    }
}