public with sharing class ExceptionHandlingPractice {
    public static void handleExceptions() {
        try {
            Contact contact = [SELECT Id FROM Contact LIMIT 1];
            System.debug(contact.Name);
        } catch (SObjectException sObjectException) {
            System.debug(sObjectException.getMessage());
        }

        try {
            insert new Contact();
        } catch (DmlException dmlException) {
            System.debug(dmlException.getMessage());
        }

        try {
            List<Contact> contacts = [SELECT Id, Name FROM Contact LIMIT 1];
            contacts.get(2);
        } catch (ListException listException) {
            System.debug(listException.getMessage());
        }

        try {
            Contact contact;
            contact.LastName = 'Error';
        } catch (NullPointerException nullPointerException) {
            System.debug(nullPointerException.getMessage());
        }
    }

    public static void handleDmlException() {
        List<Contact> contacts = new List<Contact>();

        for (Integer i = 0; i < 6; i++) {
            if (Math.mod(i, 2) == 0) {
                contacts.add(new Contact(LastName = 'Error ' + i));
            } else {
                contacts.add(new Contact());
            }
        }

        try {
            insert contacts;
        } catch (DmlException dmlException) {
            Integer numberOfFailedRow = dmlException.getNumDml();

            for (Integer i = 0; i < numberOfFailedRow; i++) {
                System.debug(dmlException.getDmlFieldNames(i));
                System.debug(dmlException.getDmlFields(i));
                System.debug(dmlException.getDmlId(i));
                System.debug(dmlException.getDmlIndex(i));
                System.debug(dmlException.getDmlMessage(i));
                System.debug(dmlException.getDmlType(i));
                System.debug('===========\n');
            }
        }
    }

    public static Boolean isCorrectAccounts(List<Account> accounts) {

        try {
            for (Account acc: accounts) {
                if (acc.NumberOfEmployees == null) {
                    throw new AccountException('The NumberOfException field is not filled!');
                }
            }
        } catch (AccountException accountException) {
            System.debug(accountException.getMessage());
        }


        return true;
    }
}