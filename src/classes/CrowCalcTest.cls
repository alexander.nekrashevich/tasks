@isTest
public class CrowCalcTest {
    private static final Integer INIT_CROWS_NUM = 10;

    @TestSetup
    public static void init() {
        addCrows(INIT_CROWS_NUM);
    }

    @isTest
    public static void testAddCrows() {
        Integer initialCrows = [SELECT COUNT() FROM Crow__c];

        Boolean addCrowsResult = CrowCalc.addCrows(10);

        Integer crowsAfter = [SELECT COUNT() FROM Crow__c] - initialCrows;

        System.assert(crowsAfter == 10, 'Wrong answer!');
        System.assert(addCrowsResult, 'Success addition not "true"');
    }

    @IsTest
    public static void testAddCrowsNegativeCase() {
        Boolean addNullCrowsResult = CrowCalc.addCrows(null);
        System.assert(!addNullCrowsResult, 'Success of addition NULL is not "false"');
    }

    @isTest
    public static void testSubtractCrows() {
        Integer initialCrows = [SELECT COUNT() FROM Crow__c];

        Boolean subtractCrowsResult = CrowCalc.subtractCrows(5);
        Boolean subtractNullCrowsResult = CrowCalc.subtractCrows(null);
        Integer leftCrows = initialCrows - [SELECT COUNT() FROM Crow__c];

        System.assert(leftCrows == 5, 'Wrong answer!');
        System.assert(subtractCrowsResult, 'The result of subtractCrows method is not "true"');
        System.assert(!subtractNullCrowsResult, 'The result of subtractCrows method is not "false"');
    }

    @isTest
    public static void testGetTotal() {
        Integer numOfCrows = [SELECT COUNT() FROM Crow__c];

        System.assert(numOfCrows == INIT_CROWS_NUM, 'The result of getTotal method is not correct');
    }

    @isTest
    public static void testResetCalc() {
        CrowCalc.resetCalc();

        Integer numOfCrows = [SELECT COUNT() FROM Crow__c];

        System.assert(numOfCrows == 0, 'The result of resetCalc method is not correct');
    }

    private static void addCrows(Integer numOfCrows) {
        List<Crow__c> toInsert = new List<Crow__c>();

        for (Integer i = 0; i < numOfCrows; i++) {
            toInsert.add(new Crow__c());
        }

        insert toInsert;
    }
}