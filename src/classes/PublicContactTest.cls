@IsTest
private class PublicContactTest {
    @IsTest
    static void testGetVolunteerNumber() {
        PublicContact publicContact = new PublicContact(
            new Contact(LastName = '123'),
            123
        );

        System.assert(publicContact.getVolunteerNumber() == 123, 'Wrong volunteerNumber!');
    }

    @IsTest
    static void testGetContactRecord() {
        Contact contact = new Contact(LastName = '123');
        PublicContact publicContact = new PublicContact(contact, 123);

        System.assert(contact == publicContact.getContactRecord(), 'Wrong contactRecord');
    }

    @IsTest
    static void testUpdateAccountId() {
        Account account = new Account(Name = '123');
        insert account;

        PublicContact publicContact = new PublicContact(new Contact(LastName = '123'), 123);
        publicContact.updateAccountId(Account.Id);

        System.assert(account.Id == publicContact.getContactRecord().AccountId, 'Wrong field AccountId in contactRecord!');
    }
}