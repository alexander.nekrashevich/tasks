public inherited sharing class PrivateContact extends Type{
    private Integer cardNumber;
    private Contact contactRecord;

    public PrivateContact(Contact contact, Integer cardNumber) {
        this.cardNumber = cardNumber;
        this.contactRecord = contact;
        this.amountOfMoney = contact.Amount__c;
        this.contactType = isPremierContact(cardNumber) ? 'Premier' : 'Person';
    }

    public Integer getCardNumber() {
        return cardNumber;
    }

    public Contact getContactRecord() {
        return contactRecord;
    }

    public void updateAccountId(Id newAccId) {
        contactRecord.AccountId = newAccId;
    }

    public static Boolean isPremierContact(Integer cardNumber) {
        List<String> digits = String.valueOf(cardNumber).split('');
        Map<String, Integer> digitCounter = new Map<String, Integer>();

        for (String digit: digits) {
            if (digitCounter.containsKey(digit)) {
                digitCounter.put(digit, digitCounter.get(digit) + 1);
            } else {
                digitCounter.put(digit, 1);
            }
        }

        for (Integer counter: digitCounter.values()) {
            if (counter >= 3) {
                return true;
            }
        }

        return false;
    }
}