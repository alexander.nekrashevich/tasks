public with sharing class Util {
    public static Integer randomIntegerInRange(Integer startRange, Integer endRange) {
        return Math.round(Math.random()*(endRange - startRange) + startRange);
    }
}