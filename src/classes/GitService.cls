public with sharing class GitService {
    public static String push(GitInterface gitInterface) {
        return gitInterface.push();
    }

    public static String pull(GitInterface gitInterface) {
        return gitInterface.pull();
    }

    public static String doCommit(GitInterface gitInterface) {
        return gitInterface.doCommit();
    }
}