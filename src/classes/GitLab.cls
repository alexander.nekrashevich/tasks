public with sharing class GitLab implements GitInterface{

    public String push() {
        return 'Pushed to GitLab.';
    }

    public String pull() {
        return 'Pulled from GitLab';
    }

    public String doCommit() {
        return 'Committed changes';
    }
}