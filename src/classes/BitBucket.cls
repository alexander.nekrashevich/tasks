public with sharing class BitBucket implements GitInterface{

    public String push() {
        return 'Pushed to BitBucket';
    }

    public String pull() {
        return 'Pulled from BitBucket';
    }

    public String doCommit() {
        return 'Committed changes';
    }
}