public class Example3 {
    public Set<String> getUniqueContactNames() {
        try {
            return getUniqueNames('Contact');
        } catch(Exception ex) {
            System.debug('Unknown Error: ' + ex.getMessage());
        }

        return null;
    }
    
    public Set<String> getUniqueAccountNames() {
        try {
            return getUniqueNames('Account');
        } catch(Exception ex) {
            System.debug('Unknown Error: ' + ex.getMessage());
        }
        
        return null;
    }
    
    private Set<String> getUniqueNames(String sObjectApiName) {
        Set<String> uniqueNames = new Set<String>();

        for(SObject sobj: Database.query('SELECT Id, Name FROM ' + sObjectApiName + ' LIMIT 50000')) {
            uniqueNames.add(String.valueOf(sobj.get('Name')));
        }
        
        return uniqueNames;
    }
}