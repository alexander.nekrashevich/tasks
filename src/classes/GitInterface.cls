public interface GitInterface {
    String push();
    String pull();
    String doCommit();
}