@IsTest
public class TestDataFactory {
    public static void createAccountsWithRelatedContacts(Integer numOfAccounts, Integer numOfContactsPerAccount) {
        List<Account> accounts = new List<Account>();

        for (Integer i = 0; i < numOfAccounts; i++) {
            accounts.add(new Account(Name = 'TestAccount ' + i));
        }
        insert accounts;

        List<Contact> contacts = new List<Contact>();
        for (Integer i = 0; i < numOfAccounts; i++) {
            for (Integer j = 0; j < numOfContactsPerAccount; j++) {
                Contact contact = new Contact(LastName = 'TestContact ' + j);
                contact.AccountId = accounts[i].Id;
                contact.Amount__c = Util.randomIntegerInRange(1000, 20000);

                contacts.add(contact);
            }
        }
        insert contacts;
    }
}