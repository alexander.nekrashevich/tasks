@IsTest
private class AccountSwitcherImplTest {

    @TestSetup
    static void init() {
        TestDataFactory.createAccountsWithRelatedContacts(10, 2);
    }

    @IsTest
    static void testGetAndSort() {
        AccountSwitcherImpl accountSwitcherImpl = new AccountSwitcherImpl();
        accountSwitcherImpl.getAndSort();

        Boolean isCorrectSorting = true;
        for (Type contact: accountSwitcherImpl.getAllContacts()) {
            if (contact instanceof PrivateContact) {
                PrivateContact privateContact = (PrivateContact) contact;

                if (privateContact.getContactRecord().Amount__c <= 10000) {
                    isCorrectSorting = false;
                }
            }

            if (contact instanceof PublicContact) {
                PublicContact publicContact = (PublicContact) contact;

                if (publicContact.getContactRecord().Amount__c > 10000) {
                    isCorrectSorting = false;
                }
            }

        }

        System.assert(accountSwitcherImpl.getAllContacts().size() == 20, 'Wrong allContacts size!');
        System.assert(isCorrectSorting, 'Incorrect sorting!');
    }

    @IsTest
    static void testSwitchAccount() {
        Map<Id, Contact> contactsByIdsBeforeSwitching = new Map<Id, Contact>([SELECT Id, AccountId FROM Contact]);

        AccountSwitcherImpl accountSwitcherImpl = new AccountSwitcherImpl();
        accountSwitcherImpl.getAndSort();

        DataResult dataResult = accountSwitcherImpl.switchAccount();

        for (PrivateContact privateContact: dataResult.nonChangedPrivateContacts) {
            System.assert(privateContact.getContactType() == 'Premier', 'DataResult must contains only "Premier" Private Contacts!');
        }

        for (PrivateContact privateContact: dataResult.changedPrivateContacts) {
            Contact contact = privateContact.getContactRecord();
            Contact oldContact = contactsByIdsBeforeSwitching.get(contact.Id); // contact before switching

            System.assert(contact.AccountId != oldContact.AccountId != null, 'AccountId of Contact was not changed, but added to changed list!');
        }

        for (PublicContact publicContact: dataResult.changedPublicContacts) {
            Contact contact = publicContact.getContactRecord();
            Contact oldContact = contactsByIdsBeforeSwitching.get(contact.Id); // contact before switching

            System.assert(contact.AccountId != oldContact.AccountId != null, 'AccountId of Contact was not changed, but added to changed list!');
        }

//        DataResult dataResultBeforeSorting = accountSwitcherImpl.switchAccount();

        System.debug('dataResult');
        System.debug(dataResult);
//        System.assert(dataResultBeforeSorting == null, 'Wrong result of switching!');
        System.assert(dataResult != null, 'Wrong result of switching!');
    }
}