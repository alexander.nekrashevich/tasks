public class Example2 {
	public Set<String> getUniqueContactNames() {
        Set<String> uniqueContactNames 	= new Set<String>();

        for (Contact cont: [SELECT Id, Name FROM Contact LIMIT 50000]) {
            uniqueContactNames.add(cont.Name);
        }
        
        return uniqueContactNames;
    }
}