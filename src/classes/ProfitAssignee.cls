public interface ProfitAssignee {
    Decimal calculateProfit();
}