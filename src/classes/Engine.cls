
public with sharing class Engine {

    public static void init() {
        List<Engine__c> engines = new List<Engine__c>();
        List<Part__c> parts = new List<Part__c>();
        List<Vehicle__c> vehicles = new List<Vehicle__c>();

        for (Integer i = 0; i < 100; i++) {
            engines.add(new Engine__c(Name = 'Engine ' + i));
        }
        insert engines;

        for (Integer i = 0; i < engines.size(); i++) {
            Part__c part = new Part__c(Name = 'Part ' + i);
            part.Engine__c = engines[i].Id;
            parts.add(part);

            Vehicle__c vehicle = new Vehicle__c(Name = 'Vehicle ' + i);
            vehicle.Engine__c = engines[i].Id;
            vehicles.add(vehicle);
        }

        List<SObject> toInsert = new List<SObject>();
        toInsert.addAll(parts);
        toInsert.addAll(vehicles);
        insert toInsert;
    }

    public static List<Engine__c> getAll() {
        return [SELECT Id, Name FROM Engine__c];
    }

    public static List<Engine__c> getAllWithVehicles() {
        return [SELECT Id, Name, (SELECT Id, Name FROM Vehicles__r) FROM Engine__c];
    }

    public static Map<Id, List<Part__c>> getVehicleIdsWithParts() {
        Map<Id, List<Part__c>> vehicleIdsWithParts = new Map<Id, List<Part__c>>();
        List<Engine__c> engines = [SELECT (SELECT Name FROM Vehicles__r), (SELECT Name FROM Parts__r) FROM Engine__c];

        for (Engine__c engine: engines) {
            for (Vehicle__c vehicle: engine.Vehicles__r) {
                vehicleIdsWithParts.put(vehicle.Id, engine.Parts__r);
            }
        }

        return vehicleIdsWithParts;
    }

    public static Map<String, List<String>> getAllNamesWithVehicleNames() {
        List<Engine__c> engines = [SELECT Name, (SELECT Name FROM Vehicles__r) FROM Engine__c];
        Map<String, List<String>> engineNamesWithVehicleNames = new Map<String, List<String>>();

        for (Engine__c engine: engines) {
            List<String> vehicleNames = new List<String>();

            for (Vehicle__c vehicle: engine.Vehicles__r) {
                vehicleNames.add(vehicle.Name);
            }

            engineNamesWithVehicleNames.put(engine.Name, vehicleNames);
        }

        return engineNamesWithVehicleNames;
    }

    public static void cloneAllRecords() {
        List<Engine__c> engines = [SELECT Id, Name, isClone__c, (SELECT Id, Name, isClone__c FROM Vehicles__r), (SELECT Id, Name, isClone__c FROM Parts__r) FROM Engine__c];
        List<SObject> toInsert = new List<SObject>();

        List<Engine__c> engineClones = new List<Engine__c>();
        List<Part__c> partClones = new List<Part__c>();
        List<Vehicle__c> vehicleClones = new List<Vehicle__c>();

        for (Engine__c engine: [SELECT Id, Name, isClone__c, (SELECT Id, Name, isClone__c FROM Vehicles__r), (SELECT Id, Name, isClone__c FROM Parts__r) FROM Engine__c]) {
            Engine__c engineClone = engine.clone();
            engineClone.Name += ' CLONE';
            engineClone.isClone__c = true;
            engineClones.add(engineClone);
        }

        insert engineClones;

        for (Integer i = 0; i < engines.size(); i++) {

            for (Part__c part: engines[i].Parts__r) {
                Part__c partClone = part.clone();
                partClone.Engine__c = engineClones[i].Id;
                partClone.Name += ' CLONE';
                partClone.isClone__c = true;
                partClones.add(partClone);
            }

            for (Vehicle__c vehicle: engines[i].Vehicles__r) {
                Vehicle__c vehicleClone = vehicle.clone();
                vehicleClone.Engine__c = engineClones[i].Id;
                vehicleClone.Name += ' CLONE';
                vehicleClone.isClone__c = true;
                vehicleClones.add(vehicleClone);
            }
        }

        toInsert.addAll(partClones);
        toInsert.addAll(vehicleClones);
        insert toInsert;
    }

    public static void removeAllClones() {
        List<Engine__c> engineClones = [SELECT Id, Name FROM Engine__c WHERE isClone__c = TRUE];
        List<Part__c> partClones = [SELECT Id, Name FROM Part__c WHERE isClone__c = TRUE];
        List<Vehicle__c> vehicleClones = [SELECT Id, Name FROM Vehicle__c WHERE isClone__c = TRUE];

        List<SObject> toDelete = new List<SObject>();
        toDelete.addAll(engineClones);
        toDelete.addAll(partClones);
        toDelete.addAll(vehicleClones);

        delete toDelete;
    }
}