public with sharing class ProfitService {
    public static Decimal totalProfit(List<ProfitAssignee> profitAssignees) {
        Decimal totalProfit = 0;

        for (ProfitAssignee profitAssignee: profitAssignees) {
            totalProfit += profitAssignee.calculateProfit();
        }

        return totalProfit;
    }


    /**
     * 7.Реализовать метод, который получает с орга списоквсех аккаунтов и их контактов,
     *   оборачивает их виннер класс и возвращает список экземпляров
     */
    public static List<ProfitAssignee> getAccountsImpl() {
//        Map<Id, Contact> contactsByIds = new Map<Id, Contact>([SELECT Id, Name, Profit__c FROM Contact LIMIT 50000]);
//        Map<Id, Account> accountsByIds = new Map<Id, Account>([SELECT Id, Name FROM Account LIMIT 50000]);
        List<ProfitAssignee> accountsProfitAssignees = new List<ProfitAssignee>();

        for (Account acc: [SELECT Id, Name, (SELECT Id, Name, Profit__c FROM Contacts) FROM Account LIMIT 50000]) {
            List<ProfitAssignee> contacts = new List<ProfitAssignee>();

            for (Contact contact: acc.Contacts) {
                contacts.add(new ContactImpl(contact.Name, contact.Profit__c));
            }

            accountsProfitAssignees.add(new AccountImpl(acc.Name, contacts));
        }

        return accountsProfitAssignees;
    }

    public static List<ProfitAssignee> getContactsImpl() {
        List<ProfitAssignee> contactsProfitAssignees = new List<ProfitAssignee>();

        for (Contact contact: [SELECT Name, Profit__c FROM Contact LIMIT 50000]) {
            contactsProfitAssignees.add(new ContactImpl(contact.Name, contact.Profit__c));
        }

        return contactsProfitAssignees;
    }
}