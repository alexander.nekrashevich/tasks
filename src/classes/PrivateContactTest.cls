@IsTest
private class PrivateContactTest {
    @IsTest
    static void testGetCardNumber() {
        Contact contact = createContact(13245);

        Integer randCardNum = Util.randomIntegerInRange(100000, 999999);
        PrivateContact privateContact = new PrivateContact(
            contact,
            randCardNum
        );

        System.assert(randCardNum == privateContact.getCardNumber(), 'Wrong number card!');
    }

    @IsTest
    static void testGetContactRecord() {
        Contact contact = createContact(132546);

        PrivateContact privateContact = new PrivateContact(
            contact,
            132
        );

        System.assert(contact == privateContact.getContactRecord(), 'Wrong contact record!');
    }

    @IsTest
    static void testUpdateAccountId() {
        Account account = new Account(Name = 'Test test');
        insert account;

        Contact contact = createContact(13456);
        PrivateContact privateContact = new PrivateContact(contact, 123);

        privateContact.updateAccountId(account.Id);

        System.assert(account.Id == privateContact.getContactRecord().AccountId, 'Wrong field AccountId in contactRecord!');
    }

    @IsTest
    static void testIsPremiereContact() {
        Boolean isPremiere = PrivateContact.isPremierContact(123141);
        Boolean isNotPremiere = PrivateContact.isPremierContact(123456);

        System.assert(isPremiere, 'PrivateContact type must be Premiere!');
        System.assert(!isNotPremiere, 'PrivateContact type must be Person!');
    }

    private static Contact createContact(Integer amount) {
        return new Contact(
            LastName = 'Smith',
            Amount__c = amount
        );
    }
}