public with sharing class SalesforceLimits {
    public static void task1() {
        List<Contact> firstContacts = [SELECT Name FROM Contact LIMIT 200];
    }

    public static void task2() {
        List<Contact> contacts = new List<Contact>();

        for (Integer i = 0; i < 200; i++) {
            Contact contact = new Contact(LastName = 'test', FirstName = 'test' + i);
            contacts.add(contact);
        }

        insert contacts;
    }

    public static void task3() {
        Set<String> allNames = new Set<String>();

        for (Contact contactItem: [SELECT FirstName FROM Contact LIMIT 50000]) {
            allNames.add(contactItem.FirstName);
        }
    }
}