public class Example4 {
    public Set<String> getUniqueContactNames() {
        try {
            return getUniqueRecordNames(getRecords('Contact'));
        } catch(Exception ex) {
            system.debug('Unknown Error: ' + ex.getMessage());
        }
        
        return null;
    }
    
    public Set<String> getUniqueAccountNames() {
        try {
            return getUniqueRecordNames(getRecords('Account'));
        } catch(Exception ex) {
            system.debug('Unknown Error: ' + ex.getMessage());
        }
        
        return null;
    }
    
    private List<SObject> getRecords(String sObjectApiName) {
        return Database.query('SELECT Id, Name FROM ' + sObjectApiName);
    }
    
    private Set<String> getUniqueRecordNames(List<SObject> sobjects) {
        Set<String> uniqueNames = new Set<String>();
        
        for(SObject sobj: sobjects) {
            uniqueNames.add(String.valueOf(sobj.get('Name')));
        }
        
        return uniqueNames;
    }
}