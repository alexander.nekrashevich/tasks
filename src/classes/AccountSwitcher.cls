public interface AccountSwitcher {
    void getAndSort();
    DataResult switchAccount();
}