public with sharing class AccountImpl implements ProfitAssignee {
    private String name;
    private List<ProfitAssignee> contacts;

    public AccountImpl(String name, List<ProfitAssignee> contacts) {
        this.name = name;
        this.contacts = contacts;
    }

    public Decimal calculateProfit() {
        Decimal profit = 0;

        for (ProfitAssignee contact: contacts) {
            profit += contact.calculateProfit();
        }

        return profit;
    }
}