public class Example1 {
	public List<String> getUniqueContactNameList() {
        Set<String> uniqueNames = new Set<String>();

        for (Contact contact: [SELECT Id, Name, Email, Phone FROM Contact LIMIT 50000]) {
            uniqueNames.add(contact.Name);
        }

        return new List<String>(uniqueNames);
    }
}