public with sharing class AccountSwitcherImpl implements AccountSwitcher{

    private static List<Type> allContacts = new List<Type>();

    public void getAndSort() {
        for (Contact[] contacts: [SELECT Id, Name, Amount__c, AccountId FROM Contact]) {
            for (Contact cont: contacts) {
                if (cont.Amount__c > 10000) {
                    allContacts.add(new PrivateContact(
                        cont,
                        Util.randomIntegerInRange(100000, 999999) // random number with 6 digits
                    ));
                } else {
                    allContacts.add(new PublicContact(
                        cont,
                        Util.randomIntegerInRange(100000, 999999) // random number with 6 digits
                    ));
                }
            }
        }
    }

    public DataResult switchAccount() {
        if (allContacts == null) {
            return null;
        }

        DataResult dataResult = new DataResult();
        List<PrivateContact> privateContacts = new List<PrivateContact>();
        List<PublicContact> publicContacts = new List<PublicContact>();

        for (Type cont: allContacts) {
            if (cont instanceof PrivateContact) {
                privateContacts.add((PrivateContact) cont);
            }

            if (cont instanceof PublicContact) {
                publicContacts.add((PublicContact) cont);
            }
        }

        while (privateContacts.size() != 0 && publicContacts.size() != 0) {

            if(privateContacts.get(0).getContactType() == 'Premier') {
                DataResult.nonChangedPrivateContacts.add(privateContacts.remove(0));
                continue;
            }

            PublicContact publicContact = publicContacts.remove(0);
            PrivateContact privateContact = privateContacts.remove(0);

            Id publicContactId = publicContact.getContactRecord().AccountId;
            Id privateContactId = privateContact.getContactRecord().AccountId;

            publicContact.updateAccountId(privateContactId);
            privateContact.updateAccountId(publicContactId);

            DataResult.changedPublicContacts.add(publicContact);
            DataResult.changedPrivateContacts.add(privateContact);
        }

        try {
            update getChangedContactsFromDataResult(dataResult);
        } catch (DmlException dmlException) {
            System.debug(dmlException.getMessage());
        }

        return dataResult;
    }

    private List<Contact> getChangedContactsFromDataResult(DataResult dataResult) {
        List<Contact> changedContacts = new List<Contact>();

        for (PrivateContact privateContact: dataResult.changedPrivateContacts) {
            changedContacts.add(privateContact.getContactRecord());
        }

        for (PublicContact publicContact: dataResult.changedPublicContacts) {
            changedContacts.add(publicContact.getContactRecord());
        }

        return changedContacts;
    }

    public List<Type> getAllContacts() {
        return allContacts;
    }
}