public inherited sharing class PublicContact extends Type{
    private Integer volunteerNumber;
    private Contact contactRecord;


    public PublicContact(Contact contact, Integer volunteerNumber) {
        this.volunteerNumber = volunteerNumber;
        this.contactRecord = contact;
        this.amountOfMoney = contact.Amount__c;
        this.contactType = 'Person';
    }

    public Integer getVolunteerNumber() {
        return volunteerNumber;
    }

    public Contact getContactRecord() {
        return contactRecord;
    }

    public void updateAccountId(Id newAccId) {
        contactRecord.AccountId = newAccId;
    }
}