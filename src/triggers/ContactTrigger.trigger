trigger ContactTrigger on Contact (before insert, before update, before delete, after insert, after update) {
    ContactTriggerHandler.handleTrigger(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap, Trigger.operationType);
}