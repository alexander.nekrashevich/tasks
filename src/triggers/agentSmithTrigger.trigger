trigger agentSmithTrigger on Contact (before update) {

    if(Trigger.isBefore && Trigger.isUpdate)  {
        AgentSmithTriggerHandler.infect((List<Contact>) Trigger.new);
    }
}