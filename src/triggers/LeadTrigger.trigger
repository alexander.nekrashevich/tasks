trigger LeadTrigger on Lead (after insert, after update) {
    if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
        LeadTriggerHandler.createSEOAndCTOforConvertedAccount(Trigger.new);
    }
}